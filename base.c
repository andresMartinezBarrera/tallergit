#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"


#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"


float desvStd(estudiante curso[]){//funcion para sacar la desviacion estandar
	int sumadepromedios=0;
	float lista[24];
	float sumatoria=0;
	for(int i=1;i<24;i++){//aca se inicia el ciclo for para poder meter todos los promedios en una lista y despues sumarlos entre si
		curso[i].prom=lista[i-1];//aca se unen nlos promedios a la lista
		
		sumadepromedios=sumadepromedios+lista[i-1];//aca se lleva a cabo la suma de los promedios entre si a traves del for
	
	}
	float promedio=sumadepromedios/23;//aca se divide la suma de los promedios por su cantidad total, la cual queda el promedio
	for (int j=1 ;j<32;j++){
		sumatoria=sumatoria+(lista[j-1]-promedio);//aca se hace la sulatoria para sacar la desviacion estandar de todos los promedios restado por el promedio de promedios por asi decirlo

	}
	float desviacionestandar = sumatoria/23;//aca se divide la sumatoria por 23 que es el numero total de promedios para sacar la desviacion estandar definitiva
		
	return desviacionestandar;//retorna de la funcion la desviacion estandar
}

float menor(float prom[]){//funcion para sacar el menor promedio
	int numeroMenor=100;//numero menor sera ocupado parasacar mas numeros menores que este, puesto que numguna nota va a superar el 100
	for(int i=1;i<24;i++){//ciclo for para que todas las notas pasen por este proceso
		if (prom[i-1]<numeroMenor){//si el promedio evaluado por el for es menor a 100, ese queda como el numero menor y si el siguiente es mas chico aun, ese queda como el menor
			numeroMenor=prom[i-1];//lo de arriba

		}
	}	
	return numeroMenor;// retorna el promedio mas bajo
	
}

float mayor(float prom[]){//funcion que retorna el promedio mas alto
	int numeroMayor=0;//el numero elegiro es 0 porque nu hay nota mas baja o igual a esa
	for (int i=1;i<24;i++){	//el ciclo for es para que todos los promedios pasen por este proceso

		if (prom[i-1]>numeroMayor){//si el prom[i-1] es mayor a numero mayor este queda como el numero mas alto, y si un sucesor es mas grande aun ese queda como el promedio mas alto
			numeroMayor=prom[i-1];//lo explicado arriba

		}
	}

	return numeroMayor;
}

void registroCurso(estudiante curso[]){
	//debe registrar las calificaciones 
	int i=0;// este entero despues va a ser usado en un ciclo while para poder ingresar las notas de todos los alumnos 

	while(i<24){//este while es para poder insertar la nota de los controles y proyectos 23 veces, osea en los 23 estudiantes
		int numeroDeControles=0;//este entero sera ocupado en un ciclo while para ingresar las notas de los controles de todos los alumnos
		int numeroDeProyectos=0;//este entero sera ocupado en un ciclo while para ingresar las notas de los proyectos de todos los alumnos
		float promedio=0;
		

		
		printf("\n\nEstudiante numero %d: %s %s %s \n\n",i, curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);// este print.
		
		printf("\n\n--------CONTROLES--------\n");

		while(numeroDeControles<6){//ciclo while para lograr ingresar nota de 6 controles
			numeroDeControles=numeroDeControles+1;//esto es para que el while no se estanque
			printf("ingrese aca la nota del control numero %d:", numeroDeControles);//print para poder ingresar la nota
			
			switch (numeroDeControles){//en estas pondiciones por ejemplo si numeroDeControles es igual a 1, la nota escrita pertenece al control 1
				case 1:scanf("%f",&curso[i].asig_1.cont1);
					promedio = promedio+((curso[i].asig_1.cont1*5)/100);break;// en la formula el control se multiplica por 5 por su porcentaje y despues se divide por 100 para despues ser sumado con las demas notas que van a pasar este mismo proceso

				case 2:scanf("%f",&curso[i].asig_1.cont2);
					promedio = promedio+((curso[i].asig_1.cont2*5)/100);break;

				case 3:scanf("%f",&curso[i].asig_1.cont3);
					promedio = promedio+((curso[i].asig_1.cont3*5)/100);break;

				case 4:scanf("%f",&curso[i].asig_1.cont4);
					promedio = promedio+((curso[i].asig_1.cont4*5)/100);break;

				case 5:scanf("%f",&curso[i].asig_1.cont5);
					promedio = promedio+((curso[i].asig_1.cont5*5)/100);break;
					
				case 6:scanf("%f",&curso[i].asig_1.cont6);
					promedio = promedio+((curso[i].asig_1.cont6*5)/100);break;
			}
		}
		printf("\n\n\n--------PROYECTOS--------\n");
		
		while(numeroDeProyectos<3){
			numeroDeProyectos=numeroDeProyectos+1;
			printf("ingrese aca la nota del proyecto numero %d:", numeroDeProyectos);
			switch (numeroDeProyectos){
				case 1:scanf("%f",&curso[i].asig_1.proy1);
				promedio = promedio+((curso[i].asig_1.proy1*20)/100);break;// en estos proyectos se multiplica la nota por un valor mayor puesto que estas notas valen mas, pero tabbien forman parte del mismo calculo de notas que los pontroles para asi despues poder sumar todas las notas y tener el promedio
				case 2:scanf("%f",&curso[i].asig_1.proy2);
				promedio = promedio+((curso[i].asig_1.proy2*20)/100);break;
				case 3:scanf("%f",&curso[i].asig_1.proy3);
				promedio = promedio+((curso[i].asig_1.proy3*30)/100);break;

			
			}
		}
		i=i+1;// esto es para que el while no se estanque	
		printf("\n--------PROMEDIO--------");
		printf("\nel promedio de del alumno es:%.1f \n\n",promedio);//aca se muestra el promedio final del estudiante
		curso[i].prom=promedio;
		printf("--------------------------------------------------------");
		
		

	}
	

}

void clasificarEstudiantes(char path[], estudiante curso[]){//esta funcion es un generador de archivos que divide al curso en aprobados y reprobados
	
		FILE *aprobado;//aca se crea el archivo de los aprobados
		if ((aprobado=fopen("aprobado.txt","w"))==NULL){//condicion que sale cuando hay problemas para abrir el archivo//
			printf("error al cargar el archivo");
			exit(0);
		}
		else{
			for (int i=1;i<24;i++){//si no hubo ningun probrema se prosigue a analizar de los 24 estudiantes quienes pasaron y quien no
				if(curso[i].prom>=4){//condicion que dise que si el promedio del alumno fue mayor o igual a 4, este abrueba
				fprintf(aprobado,"%s %s %s\n\n",curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM);}//aca se inpreime el nombre y apellidos del alumno
			}
			printf("archivo creado con exito." );
			fclose(aprobado);//aca se cierra el archivo 
		}
		
		
		FILE *reprobado;//aca se crea el archivo de los rebrobados
		if ((reprobado=fopen("reprobado.txt","w"))==NULL){//aca si hay problemas con el archivo, el programa dice que tiene problemas para abrirlo
			printf("error al cargar el archivo");
			exit(0);
		}
		else{
		for (int j=1;j<24;j++){//aca ocurre exactamento lo mismo que con el archivo de aprobados, pero con la diferencia de que la condicion agarra la nota solo si es menor a 4
			if (curso[j].prom<4){
				fprintf(reprobado,"%s %s %s\n\n",curso[j].nombre,curso[j].apellidoP,curso[j].apellidoM);}}
		printf("archivo creado con exito." );
		fclose(reprobado); 
		}
		
		

		

		
}


void metricasEstudiantes(estudiante curso[]){//aca se ejecutan lo que deberian ejecutar las funciones
	float lista[24];//aca se asigna el tama;o de una lista que mas adelante sera util
	float numeroMayor=0;
	float numeroMenor=100;
	float sumadepromedios=0;
	float sumatoria= 0;
	printf("---------PROMEDIOS---------\n");
	for(int i=1;i<24;i++){//eeste for sirve para hacer las acciones en todos los estudiantes y tener las estadisticas
		
		printf("%s %s %s: %.1f \n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM,curso[i].prom);//esto imprime el nombre de todos llos alumnos y sus respectivos promedios
		lista[i-1]=curso[i].prom;//esto le asigna a el promedio de un alumno la posicion en una lista
		if (lista[i-1]>numeroMayor){//si aquel numero es mayor a numeroMayor, este pasa a ser el numero mas grande, pero si uno de sus numeros sucesores es mas grande que aquel, el numeroMayor pasa a ser el sucesor
			numeroMayor=lista[i-1];

		}
		if (lista[i-1]<numeroMenor){//aca pasa lo mismo pero en el caso contrario, osea para buscar el numero menor
			numeroMenor=lista[i-1];

		}
		sumadepromedios=sumadepromedios+lista[i-1];//lo que hace esto es sumar todos los promedios de todos los estudiantes
	
		
	}
	float promedio=sumadepromedios/23;//esto divide la suma de todos los promedios por el numero total de estudiantes para asi poder tener el promedio de total de notas de todos los alumnos
	for (int j=1 ;j<23;j++){
		sumatoria=sumatoria+(lista[j-1]-promedio);//esto crea la sumatoria para asi poder calcular la desiacion estandar

	}
	float desviacionestandar = sumatoria/23;//este es el ultimo pso para poder calcular la desviacion estandar que e dividir la sumatoria por la cantidad total de alumnos
	printf("\nmayor promedio: %.1f\n",numeroMayor);
	printf("menor promedio: %.1f\n",numeroMenor);
	printf("desviacion estandar: %.1f\n",desviacionestandar);

}





void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion ); 
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					
					
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; }
